import sys
sys.path.append('/usr/local/lib/python2.7/site-packages/')

import dash
import dash_auth
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from data_manager import DataManager

dm = DataManager('./vgsales.csv')
dm.group_sales_by(column_name='Year')
orig_df = dm.data

dm.reset_data()
dm.group_sales_by(column_name='Platform')
platforms = [p for p in dm.data['Platform']]

region_val_label_dict = {
    'Global_Sales': 'Global',
    'NA_Sales': 'North America',
    'EU_Sales': 'Europe',
    'JP_Sales': 'Japan',
    'Other_Sales': 'Other'
}

categories = ['Genre', 'Platform', 'Publisher', 'Region']
bubble_categories = ['Genre', 'Platform', 'Publisher']

# Keep this out of source code repository - save in a file or a database
VALID_USERNAME_PASSWORD_PAIRS = [
    ['hello', 'world']
]

app = dash.Dash('auth')
auth = dash_auth.BasicAuth(
    app,
    VALID_USERNAME_PASSWORD_PAIRS
)

app.layout = html.Div([

    html.Div([
        dcc.Checklist(
            id='region-checklist',
            options=[{'label': label, 'value': value} for value, label in region_val_label_dict.items()],
            values=['Global_Sales'],
            labelStyle={'display': 'inline-block'}
        ),
        html.Div([
            dcc.RangeSlider(
                id='chart-yrrange-slider',
                min=int(orig_df['Year'].min()),
                max=int(orig_df['Year'].max()),
                step=None,
                marks={int(year): str(int(year)) for year in orig_df['Year'].unique()},
                value=[int(orig_df['Year'].min()), int(orig_df['Year'].max())]
            )
            ], style={
                'padding': '10px 1px 30px 1px'
            }),
        dcc.Graph(id='graph-chart')
        ], style={
            'borderBottom': 'thin lightgrey solid',
            'backgroundColor': 'rgb(250, 250, 250)',
            'padding': '20px 20px'
        }),

    html.Div([
        html.Div([
            dcc.Dropdown(
                id='category-dropdown-pie',
                options=[{'label': category, 'value': category} for category in categories],
                value='Genre'
            ),
            ], style={
                'width': '50%',
                'padding': '10px 10px'
            }),
        html.Div([
            dcc.RangeSlider(
                id='pie-yrrange-slider',
                min=int(orig_df['Year'].min()),
                max=int(orig_df['Year'].max()),
                step=None,
                marks={int(year): str(int(year)) for year in orig_df['Year'].unique()},
                value=[int(orig_df['Year'].min()), int(orig_df['Year'].max())]
            )
            ], style={
                'padding': '10px 1px 30px 1px'
            }),
        dcc.Graph(id='graph-pie'),
        ], style={
            'borderBottom': 'thin lightgrey solid',
            'backgroundColor': 'rgb(250, 250, 250)',
            'padding': '20px 20px'
        }),

    html.Div([
        html.Label('Platforms:', style={'font-weight': 'bold'}),
        dcc.Checklist(
            id='platform-checklist',
            options=[{'label': platform, 'value': platform} for platform in platforms],
            values=[platform for platform in platforms],
            labelStyle={'display': 'inline-block'}
        ),
        html.Label('Sales by region:', style={'font-weight': 'bold'}),
        dcc.RadioItems(
            id='region-radioitems',
            options=[{'label': label, 'value': value} for value, label in region_val_label_dict.items()],
            value='Global_Sales',
            labelStyle={'display': 'inline-block'}
        ),
        dcc.Graph(id='graph-trend')
        ], style={
            'borderBottom': 'thin lightgrey solid',
            'backgroundColor': 'rgb(250, 250, 250)',
            'padding': '20px 20px'
        }),

    html.Div([
        html.Div(children='''
            The size of the bubbles help in a comparative analysis 
            of the global sales for publishers/platforms/genre across years or within a year.
        '''),
        html.Div([
            dcc.Dropdown(
                id='category-dropdown-bubble',
                options=[{'label': category, 'value': category} for category in bubble_categories],
                value='Genre'
            ),
            ], style={
                'width': '50%',
                'padding': '10px 10px'
            }),
        html.Div([
            dcc.Graph(id='graph-bubble'),
            ], style={
                'padding': '2px 10px'
            })
        ], style={
            'borderBottom': 'thin lightgrey solid',
            'backgroundColor': 'rgb(250, 250, 250)',
            'padding': '20px 20px'
        })

])


@app.callback(
    dash.dependencies.Output('graph-chart', 'figure'),
    [dash.dependencies.Input('chart-yrrange-slider', 'value'),
     dash.dependencies.Input('region-checklist', 'values')])
def update_chart(selected_year_range, selected_region_vals):
    dm.reset_data()
    dm.filter_by_range(column_name='Year', min_val=selected_year_range[0], max_val=selected_year_range[1], include_max=True)
    dm.group_sales_by(column_name='Year')
    df = dm.data

    traces = []
    for selected_region_val in selected_region_vals:
        traces.append(go.Scatter(
            x=df['Year'],
            y=df[selected_region_val],
            text=region_val_label_dict[selected_region_val],
            mode='lines+markers',
            opacity=0.7,
            name=region_val_label_dict[selected_region_val]
        ))

    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'title': 'Year'},
            yaxis={'title': 'Sales'},
            margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
            legend={'x': 0, 'y': 1},
            hovermode='closest'
        )
    }

@app.callback(
    dash.dependencies.Output('graph-pie', 'figure'),
    [dash.dependencies.Input('pie-yrrange-slider', 'value'),
    dash.dependencies.Input('category-dropdown-pie', 'value')])
def update_pie(selected_year_range, selected_category):
    dm.reset_data()
    dm.filter_by_range(column_name='Year', min_val=selected_year_range[0], max_val=selected_year_range[1],
                       include_max=True)
    values = []
    labels = []
    if selected_category != 'Region':
        dm.group_sales_by(column_name=selected_category)
        df = dm.data
        df['Agreg_Sales'] = df.sum(axis=1)
        total_agreg_sales = df['Agreg_Sales'].sum()

        other_value = 0.0
        for i, item in enumerate(df[selected_category]):
            if (df['Agreg_Sales'][i]/total_agreg_sales)*100 < 1:
                other_value += df['Agreg_Sales'][i]
            else:
                values.append(df['Agreg_Sales'][i])
                labels.append(item)
        if other_value > 0.0:
            values.append(other_value)
            labels.append("Other")
    else:
        dm.group_sales_by(column_name='Year')
        df = dm.data
        for val, label in region_val_label_dict.items():
            if label == "Global":
                continue
            values.append(df[val].sum())
            labels.append(label)

    data = [
        {
            'values': values,
            'labels': labels,
            'type': 'pie',
        },
    ]

    return {
            'data': data,
            'layout': {
                'margin': {
                    'l': 30,
                    'r': 0,
                    'b': 30,
                    't': 0
                },
                'legend': {'x': 0, 'y': 1}
            }
    }

@app.callback(
    dash.dependencies.Output('graph-trend', 'figure'),
    [dash.dependencies.Input('platform-checklist', 'values'),
     dash.dependencies.Input('region-radioitems', 'value')])
def update_trend(selected_platforms, selected_region):
    dm.reset_data()
    dm.group_sales_by(column_name=['Platform', 'Year'])
    df = dm.data

    traces = []
    for platform in selected_platforms:
        df_filtered = df[df['Platform'] == platform]
        traces.append(go.Scatter(
            x=df_filtered['Year'],
            y=df_filtered[selected_region],
            mode='lines+markers',
            opacity=0.7,
            name=platform
        ))

    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'title': 'Year'},
            yaxis={'title': selected_region},
            margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
            legend={'x': 0, 'y': 1},
            hovermode='closest'
        )
    }

@app.callback(
    dash.dependencies.Output('graph-bubble', 'figure'),
    [dash.dependencies.Input('category-dropdown-bubble', 'value')])
def update_bubble(selected_category):
    dm.reset_data()
    dm.group_sales_by(column_name=[selected_category, 'Year'])
    dm.sort(column_name=['Global_Sales'], ascending=False)
    df = dm.data.head(100)

    trace = go.Scatter(
                x=df['Year'],
                y=df[selected_category],
                text=df['Global_Sales'],
                mode='markers',
                marker=dict(
                    size=df['Global_Sales'],
                    sizemode='area',
                    sizeref=2.*max(df['Global_Sales'])/(20.**2),
                    sizemin=4
                )
            )

    return {
        'data': [trace],
        'layout': go.Layout(
            xaxis={'title': 'Year'},
            yaxis={'title': selected_category},
            margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
            legend={'x': 0, 'y': 1},
            hovermode='closest'
        )
    }

app.css.append_css({'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'})

if __name__ == '__main__':
    app.run_server()
